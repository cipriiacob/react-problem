var webpack = require('webpack');

module.exports = {
    // devtool: 'source-map',
    devtool: 'eval',
    entry: __dirname + "/app/Main.js",
    output: {
        path: __dirname + "/public",
        filename: "bundle.js",
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    "css-loader"
                ]
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ],
    resolve: {
        extensions: ['.js', '.jsx', '.css']
    },
    devServer: {
        contentBase: "./public",
        historyApiFallback: true,
        inline: true,
        port: 3000,
        proxy: {
            '/url/*': {
                target: "http://localhost:8080",
                secure: false
            }
        }
    }
};