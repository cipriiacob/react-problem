import React, {Component} from 'react';
import {render} from 'react-dom';
import {Router, Route, IndexRoute, Link, hashHistory} from "react-router";
import "whatwg-fetch";
import App from './App.js';

class Main extends Component {
    render() {
        console.log("Render");
        return this.props.children;
    }
}

render((
    <Router history={hashHistory}>
        <Route path="/main" component={App}>
            <IndexRoute component={App}/>
        </Route>
    </Router >
)
, document.getElementById('root'));